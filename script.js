const burgerBtn = document.querySelector('.burger'),
      menu = document.querySelector('.menu'),
      menuItem = document.querySelectorAll('.menu__list-item');

burgerBtn.addEventListener('click', () => {
    menu.classList.toggle('menu__active');
});
menuItem.forEach(item => {
    item.addEventListener('click', ()=>{
        menu.classList.remove('menu__active');
    });
});